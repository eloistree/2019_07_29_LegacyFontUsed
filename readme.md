# How to use: 2019_07_29_LegacyFontUsed   
   
Add the following line to the [UnityRoot]/Packages/manifest.json    
``` json     
"be.eloiexperiments.legacyfontused":"https://gitlab.com/eloistree/2019_07_29_LegacyFontUsed.git",    
```    
--------------------------------------    
   
Feel free to support my work: http://patreon.com/eloistree   
Contact me if you need assistance: http://eloistree.page.link/discord   
   
--------------------------------------    
``` json     
{                                                                                
  "name": "be.eloiexperiments.legacyfontused",                              
  "displayName": "LegacyFontUsed",                        
  "version": "0.0.1",                         
  "unity": "2018.1",                             
  "description": "Library of Font used in my previous project. Mainly taken from the web. Not sure that all license are copyright free. Don't abuse of this Reperitory.",                         
  "keywords": ["Script","Tool","Font","FromWeb"],                       
  "category": "Script",                   
  "dependencies":{}     
  }                                                                                
```    